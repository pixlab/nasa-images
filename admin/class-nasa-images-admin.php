<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Nasa_Images
 * @subpackage Nasa_Images/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Nasa_Images
 * @subpackage Nasa_Images/admin
 * @author     Eugene Chernonor <eugeneweblab@gmail.com>
 */
class Nasa_Images_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $nasa_images    The ID of this plugin.
	 */
	private $nasa_images;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $nasa_images       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $nasa_images, $version ) {
		$this->nasa_images = $nasa_images;
		$this->version = $version;
	}
	
	
	/**
	 * Flush rewrite rules
	 */
	public function maybe_flush_rewrite_rules() {
		
		if ( get_option( 'nasa_images_flush_rewrite_rules_flag' ) ) {
			flush_rewrite_rules();
			delete_option( 'nasa_images_flush_rewrite_rules_flag' );
		}
		
	}
	
	
	/**
	 * Create new NASA Image post
	 *
	 * @param $postData
	 *
	 * @return bool|int|WP_Error
	 */
	public function create_new_nasa_image_post(){
		
		$request  = new Nasa_Images_Requests();
		$postData = $request->get_picture_of_the_day();
		
		if ( empty($postData) ) return false;
		
		$post_date      = filter_var($postData->date, FILTER_SANITIZE_NUMBER_INT);
		$post_content   = filter_var($postData->explanation, FILTER_SANITIZE_STRING);
		$image_url      = filter_var($postData->hdurl, FILTER_SANITIZE_URL);
		
		$post_data = array(
			'post_type'     => 'nasa-images',
			'post_title'    => $post_date,
			'post_content'  => $post_content,
			'post_status'   => 'publish',
			'post_author'   => 1,
		);
		
		$postID = wp_insert_post( $post_data );
		
		
		if ( is_wp_error($postID) ) {
			return $postID->get_error_message();
		}
		else {
			//$attachment = new Nasa_Images_Attachments();
			return Nasa_Images_Attachments::upload_file_to_uploads($image_url, $postID);
		}
		
	}
	

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Nasa_Images_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Nasa_Images_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->nasa_images, plugin_dir_url( __FILE__ ) . 'css/nasa-images-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Nasa_Images_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Nasa_Images_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->nasa_images, plugin_dir_url( __FILE__ ) . 'js/nasa-images-admin.js', array( 'jquery' ), $this->version, false );

	}

}
