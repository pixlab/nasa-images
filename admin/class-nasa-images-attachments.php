<?php

class Nasa_Images_Attachments {
	
	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
	
	
	}
	
	
	/**
	 * Upload image to database and uploads folder
	 *
	 * @param $file
	 * @param $parent_post_id
	 *
	 * @return bool|int
	 */
	static function upload_file_to_uploads( $file, $parent_post_id  ) {
		
		$filename    = basename($file);
		
		require_once ABSPATH . 'wp-admin/includes/media.php';
		require_once ABSPATH . 'wp-admin/includes/file.php';
		require_once ABSPATH . 'wp-admin/includes/image.php';
		
		$img_id = media_sideload_image( $file, $parent_post_id, $filename, 'id' );
		
		if ( $img_id ) {
			return set_post_thumbnail( $parent_post_id, $img_id );
		}
		
		return false;
		
	}
	
	
	
}