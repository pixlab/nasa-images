<?php

class Nasa_Images_Post_Types {
	
	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $nasa_images    The ID of this plugin.
	 */
	private $nasa_images;
	
	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;
	
	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $nasa_images       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $nasa_images, $version ) {
		
		$this->nasa_images = $nasa_images;
		$this->version     = $version;
		
	}
	
	public function setup_post_type(){
		
		$labels = array(
			'name'               => __( 'NASA Images', 'nasa-images' ),
			'singular_name'      => __( 'NASA Image', 'nasa-images' ),
			'add_new'            => _x( 'Add New NASA Image', 'nasa-images', 'nasa-images' ),
			'add_new_item'       => __( 'Add New NASA Image', 'nasa-images' ),
			'edit_item'          => __( 'Edit NASA Image', 'nasa-images' ),
			'new_item'           => __( 'New NASA Image', 'nasa-images' ),
			'view_item'          => __( 'View NASA Image', 'nasa-images' ),
			'search_items'       => __( 'Search NASA Images', 'nasa-images' ),
			'not_found'          => __( 'No NASA Images found', 'nasa-images' ),
			'not_found_in_trash' => __( 'No NASA Images found in Trash', 'nasa-images' ),
			'parent_item_colon'  => __( 'Parent NASA Image:', 'nasa-images' ),
			'menu_name'          => __( 'NASA Images', 'nasa-images' ),
		);
		
		$args   = array(
			'labels'              => $labels,
			'hierarchical'        => false,
			'description'         => 'description',
			'taxonomies'          => array(),
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => null,
			'show_in_nav_menus'   => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'has_archive'         => true,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => true,
			'capability_type'     => 'post',
			'supports'            => array(
				'title',
				'editor',
				'author',
				'thumbnail',
				'excerpt',
				'custom-fields',
				'trackbacks',
				'comments',
				'revisions',
				'page-attributes',
				'post-formats',
			),
		);
		
		register_post_type( 'nasa-images', $args );
	
	}
	
}