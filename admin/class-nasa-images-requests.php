<?php

class Nasa_Images_Requests {
	
	private $api_key;
	private $daily_image_url;
	
	
	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
	
		$this->api_key         = 'JLA5qqYzs3Yzfvw2xoc3rPK5EFSq8UDe0nro4WBK';
		$this->daily_image_url = 'https://api.nasa.gov/planetary/apod';
	
	}
	
	
	/**
	 * Getting daily NASA image
	 *
	 * @return array|mixed|null|object
	 */
	public function get_picture_of_the_day() {
		return $this->make_request('daily-image', $this->daily_image_url);
	}
	
	
	/**
	 * Making external requests
	 *
	 * @param      $type
	 * @param null $url
	 *
	 * @return array|mixed|null|object
	 */
	private function make_request( $type, $url = null ) {
		
		// We can add additional types of requests
		switch( $type ) {
			case 'daily-image':
				$main_url = $url.'?api_key='.$this->api_key;
				$args = array(
							"method" => "GET",
						);
				
				break;
		}
		
		$response = wp_remote_request( $main_url, $args );
		
		try {
			// Note that we decode the body's response since it's the actual JSON feed
			$json = json_decode( $response['body'] );
		} catch ( Exception $ex ) {
			$json = null;
		}
		
		return $json;
		
	}
	
}