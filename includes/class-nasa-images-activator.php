<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Nasa_Images
 * @subpackage Nasa_Images/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Nasa_Images
 * @subpackage Nasa_Images/includes
 * @author     Eugene Chernonor <eugeneweblab@gmail.com>
 */
class Nasa_Images_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		/**
		 * Add a flag that will allow to flush the rewrite rules when needed.
		 */
		if ( ! get_option( 'nasa_images_flush_rewrite_rules_flag' ) ) {
			add_option( 'nasa_images_flush_rewrite_rules_flag', true );
		}
		
		
		/**
		 * Cron for getting new NASA Images on daily base
		 */
		if ( ! wp_next_scheduled( 'nasa_images_cron_get_nasa_picture' ) ) {
			wp_schedule_event( time(), 'hourly', 'nasa_images_cron_get_nasa_picture' );
		}
	}

}
