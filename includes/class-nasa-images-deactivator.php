<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Nasa_Images
 * @subpackage Nasa_Images/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Nasa_Images
 * @subpackage Nasa_Images/includes
 * @author     Eugene Chernonor <eugeneweblab@gmail.com>
 */
class Nasa_Images_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		
		/**
		 * Deactivate Cron for getting new NASA Images on daily base
		 */
		wp_clear_scheduled_hook( 'nasa_images_cron_get_nasa_picture' );
		
	}

}
