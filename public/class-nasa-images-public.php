<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Nasa_Images
 * @subpackage Nasa_Images/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Nasa_Images
 * @subpackage Nasa_Images/public
 * @author     Eugene Chernonor <eugeneweblab@gmail.com>
 */
class Nasa_Images_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $nasa_images    The ID of this plugin.
	 */
	private $nasa_images;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $nasa_images       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $nasa_images, $version ) {

		$this->nasa_images = $nasa_images;
		$this->version     = $version;

	}
	
	
	/**
	 * NASA Images carousel shorcode
	 *
	 * @param $atts
	 */
	public function nasa_images_carousel( $atts ){
		
		extract(shortcode_atts(array(
			'amount' => '10',
		), $atts));
		
		// WP_Query arguments
		$args = array(
			'post_type'              => array( 'nasa-images' ),
			'post_status'            => array( 'publish' ),
			'posts_per_page'         => $amount,
			'posts_per_archive_page' => $amount,
			'order'                  => 'DESC',
			'orderby'                => 'date',
		);

		// The Query
		$query = new WP_Query( $args );

		// The Loop
		if ( $query->have_posts() ) {
			
			echo '<div class="nasa-images-carousel js-nasa-images-carousel">';
			
			while ( $query->have_posts() ) {
				$query->the_post();
				
				$attachment_url = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), array(300,300) );
				$url = $attachment_url['0'];
				
				echo '<div>';
					echo '<img src="'.$url.'" alt="thumb" />';
				echo '</div>';
				
			}
			
			echo '</div>';
		} else {
			// no posts found
		}

		// Restore original Post Data
		wp_reset_postdata();
	
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Nasa_Images_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Nasa_Images_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( 'slick-slider-styles', plugin_dir_url( __FILE__ ) . 'css/slick.css', array(), $this->version, 'all' );
		
		wp_enqueue_style( $this->nasa_images, plugin_dir_url( __FILE__ ) . 'css/nasa-images-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Nasa_Images_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Nasa_Images_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( 'slick-slider-js', plugin_dir_url( __FILE__ ) . 'js/slick.min.js', array( 'jquery' ), $this->version, true );
		
		wp_enqueue_script( $this->nasa_images, plugin_dir_url( __FILE__ ) . 'js/nasa-images-public.js', array( 'jquery', 'slick-slider-js' ), $this->version, true );

	}

}
