(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 */
	document.addEventListener("DOMContentLoaded", () => {

		const NASA_IMAGES_CAROUSEL = document.querySelector('.js-nasa-images-carousel');

		(NASA_IMAGES_CAROUSEL)
		&& $(NASA_IMAGES_CAROUSEL).slick({
			infinite: true,
			slidesToShow: 3,
			slidesToScroll: 3,
			speed: 300,
			lazyLoad: 'ondemand',
			autoplay: true,
			autoplaySpeed: 2000,
		});

	});

})( jQuery );
